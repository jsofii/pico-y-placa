# PicoPlaca

> Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.

> Si deseas probarlo entra en este link: https://jsofii.gitlab.io/pico-y-placa/

Software predictor de pico y placa. Dado una placa vehicular, una fecha en formato (DD/MM/AAAA) y una hora, informará si el 
vehículo especificado puede circular o no en la fecha y hora ingresadas.

Las reglas pueden ser modificadas según la conveniencia del usuario mediante el archivo  que se encuentra en la carpeta compartida assets del proyecto.

> Por defecto este archivo contiene las reglas de pico y placa que estuvieron vigentes en Ecuador, mismas que se detallan a continuación:
   
| Día de la semana | Último dígito de la placa del vehículo |
| ------ | ------ |
| Lunes | 1,2 |
| Martes | 3,4 |
| Miércoles | 5,6 |
| Jueves | 7,8 |
| Viernes | 9,0 |    

## ¿Cómo modificar las reglas?

Si deseas utilizar el predictor con una validación distinta a la que se tiene por defecto puedes modificar el archivo "reglasCatalogo.json" ubicado en la carpeta assets de la siguiente manera:

### Estructura 
    `{
        "dia": {
            "numero": 1,
            "nombre": "Lunes"
        },
        "ultimoDigito": [1,2,9],
        "horario": [
            {
                "horaInicio": "07:00",
                "horaFin": "09:30"
            },
            {
                "horaInicio": "16:00",
                "horaFin": "19:30"
            }
        ]
    }`


En **dia** se hace referencia al día en el que se quiere aplicar la restricción.
En **ultimoDigito** se tiene un array de números, aquí se debe poner los últimos dígitos de las placas a las que aplicará la restricción.
En **horario** se tiene un arreglo de horarios, se puede añadir los intervalos que se desee, cada elemento de este arreglo tiene un hora inicio y hora fin. Esto quiere decir que dentro de los intervalos de tiempo de cada horario no se podrá circular.
# Deploy y construcción automática
- Rama develop: al hacer cambios en esta rama se correrá la fase de build
- Rama master (producción): al hacer cambios en es rama se correrá la fase de build y también la fase de deploy (lanzar estos nuevos cambios a producción)


# Levantar el proyecto
> npm i // instalar dependencias

> ng serve //levantar el proyecto

> ng test //correr pruebas unitarias
