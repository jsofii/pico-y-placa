import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-luz',
  templateUrl: './luz.component.html',
  styleUrls: ['./luz.component.scss']
})
export class LuzComponent implements OnInit {
  @Input() color: string;
  public ngOnChanges(changes: SimpleChanges) {
    if ('color' in changes) {
      this.estilo={background: this.color}
     }
}  
  estilo = {}
  mostrarMensaje= false;
  constructor() { }

  ngOnInit(): void {
    this.estilo={background: this.color}
  }

}
