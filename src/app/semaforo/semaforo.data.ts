

export interface Regla{
   dia: Dia,
   ultimoDigito: number[],
   horario: Horario[]
}
export interface Dia{
    numero: number,
    nombre: string
}

export interface Horario{
    horaInicio: string,
    horaFin: string
}

