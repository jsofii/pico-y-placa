import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common'
import {
  Dia, Regla, Horario
} from './semaforo.data';
import { Observable } from 'rxjs';
import { semaforoService } from './semaforo.interface'
import { parse } from 'date-fns';


@Injectable({
  providedIn: 'root'
})

export class SemaforoPicoPlacaService implements semaforoService {
  reglas = [] as Regla[]
  luzColorRojo = "white"
  luzColorVerde = "white"
  constructor(private http: HttpClient) {

  }
  prenderLuz(color: string) {
    if(color=="roja"){
      this.luzColorRojo="red"
      this.luzColorVerde="white"
    }else{
      this.luzColorRojo="white"
      this.luzColorVerde="green"
    }
  }
  apagarLuces() {
    this.luzColorRojo = "white"
    this.luzColorVerde = "white"
  }

  cargarCatalogoReglasPicoPlaca(): Observable<Regla[]> {

    return this.http.get<Regla[]>("./assets/reglasCatalogo.json")

  }
  validarDia(diaConsultado: number) {
    if (this.reglas.length > 0) {
      var existeRestriccionParaEsteDia = this.reglas.some(regla => regla.dia.numero = diaConsultado)
      return existeRestriccionParaEsteDia
    } else {
      return null
    }
  }
  tieneRestriccionHora(regla: Regla, horaIngresada: string): boolean {
    const tieneRestriccion = regla.horario.some(horario=>{
      return this.estaFechaEnRango(horaIngresada, horario.horaInicio, horario.horaFin)
    })
    return tieneRestriccion
  }
  estaFechaEnRango(horaIngresada: string, horaInicio:string, horaFin:string):boolean{
    const timeHoraIngresada= this.parsearStringHoraAFecha(horaIngresada).getTime()
    const timeHoraInicio= this.parsearStringHoraAFecha(horaInicio).getTime()
    const timeHoraFin= this.parsearStringHoraAFecha(horaFin).getTime()
    return (timeHoraIngresada>= timeHoraInicio && timeHoraIngresada <= timeHoraFin)
  }

  parsearStringHoraAFecha(hora: string): Date {
    var fecha = new Date();
    var partesHora = hora.split(":")
    fecha.setHours(Number(partesHora[0]), Number(partesHora[1]), 0, 0)
    return fecha
  }
  tieneRestricccionPlaca(regla: Regla, placa:string): boolean{
    const tieneRestriccion= regla.ultimoDigito.some(digito=> digito== Number(this.obtenerUltimoCaracter(placa)))
    return tieneRestriccion
  }
  obtenerUltimoCaracter(valor: string) {
    return valor.slice(valor.length - 1)
  }
  parsearFechaADate(fecha: string) {
    const fechaParseada = parse(fecha,
      'dd/MM/yyyy',
      new Date());
    if (!isNaN(fechaParseada.getDay())) {
      return fechaParseada
    }
  }
  existeReglaParaDia(fecha: Date){
    const existeReglaParaDia = this.reglas.find(regla=> regla.dia.numero == fecha.getDay())
    return existeReglaParaDia
  }
  puedeSalirEnPicoPlaca(consulta:any){
    let puedeSalir= true
    const fechaIngresada= this.parsearFechaADate(consulta.fecha)
    const reglaParaDiaSeleccionado= this.existeReglaParaDia(fechaIngresada)
    if(reglaParaDiaSeleccionado!=null){
      const tieneRestriccionPlaca = this.tieneRestricccionPlaca(reglaParaDiaSeleccionado, consulta.placa )
      if(tieneRestriccionPlaca){
        const tieneRestriccionHora=this.tieneRestriccionHora(reglaParaDiaSeleccionado, consulta.hora)
        if(tieneRestriccionHora){
          puedeSalir = false
        }
      }
    }
    return puedeSalir
  }
 
 


}
