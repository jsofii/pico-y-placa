import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Regla } from './semaforo.data';
import { SemaforoPicoPlacaService } from './semaforo.service'
import { parse } from 'date-fns';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';


export function validadorDeFecha(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {

    const valorFecha = control.value;

    if (!valorFecha) {
      return null;
    }
    let fechaParseada = parse(valorFecha,
      'dd/MM/yyyy',
      new Date());



    return isNaN(fechaParseada.getDay()) ? { fechaNoValida: true } : null;
  }
}
@Component({
  selector: 'app-semaforo',
  templateUrl: './semaforo.component.html',
  styleUrls: ['./semaforo.component.scss'],
  providers: [SemaforoPicoPlacaService]
})
export class SemaforoComponent implements OnInit {
  reglas: Regla[] = [] as Regla[]


  constructor(public _SemaforoPicoPlacaService: SemaforoPicoPlacaService, private formBuilder: FormBuilder) { }

  consultaPicoPlacaForm: FormGroup;
  patronValidadorFormatoFecha = ""
  ngOnInit(): void {
    this.traerCatalogoReglas()
    this.inicializarFormulario()

  }

  inicializarFormulario() {
    this.consultaPicoPlacaForm = this.formBuilder.group({
      placa: ["", [Validators.required, Validators.pattern('^[a-zA-Z0-9_.-]*[0-9]$')]],
      fecha: ["", [Validators.required, Validators.pattern('^[0-9]{2}[\/][0-9]{2}[\/][0-9]{4}$'), validadorDeFecha()]],
      hora: ["", Validators.required]
    });
  }
  traerCatalogoReglas() {

    this._SemaforoPicoPlacaService.cargarCatalogoReglasPicoPlaca().subscribe(
      data => {
        this._SemaforoPicoPlacaService.reglas = data
      }
    )
  }
  consultarPicoPlaca() {
    const puedeSalir=this._SemaforoPicoPlacaService.puedeSalirEnPicoPlaca(this.consultaPicoPlacaForm.value)
    if(puedeSalir){
      this._SemaforoPicoPlacaService.prenderLuz("verde")
    }else{
      this._SemaforoPicoPlacaService.prenderLuz("roja")
    }
   
  }




}




