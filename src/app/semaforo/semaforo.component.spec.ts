import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';

import { SemaforoComponent } from './semaforo.component';
import { Regla } from './semaforo.data';
import { SemaforoPicoPlacaService } from './semaforo.service';

describe('SemaforoComponent', () => {
  let componente: SemaforoComponent;
  let fixture: ComponentFixture<SemaforoComponent>;
  let servicio;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule,
        ReactiveFormsModule,
         ],
      declarations: [ SemaforoComponent ]
    })
    .compileComponents();
    servicio= TestBed.get(SemaforoPicoPlacaService)
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemaforoComponent);
    componente = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('debe crearse', () => {
    expect(componente).toBeTruthy();
  });

  it('Debe llamar a SemaforoPicoyPlacaService y al método cargarCatalogoReglasPicoPlaca()'), ()=>{
    let mockReglas: Regla[] = [{
      dia: {
        numero: 1,
        nombre: "Lunes"
      },
      ultimoDigito: [1, 2],
      horario: [
        {
          horaInicio: "07:00",
          horaFin: "09:30"
        },
        {
          horaInicio: "16:00",
          horaFin: "19:30"
        }
      ]
    },
    {
      dia: {
        numero: 2,
        nombre: "Martes"
      },
      ultimoDigito: [3, 4],
      horario: [
        {
          horaInicio:" 07:00",
          horaFin: "09:30"
        },
        {
          horaInicio: "16:00",
          horaFin: "19:30"
        }
      ]
    }]
    const reglas=spyOn(servicio, 'cargarCatalogoReglasPicoPlaca').and.callFake(user=>{
      return of(mockReglas)
    })
    componente.ngOnInit()
    expect(reglas).toHaveBeenCalled()

  }
});
