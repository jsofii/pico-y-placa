import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { Regla } from './semaforo.data';

import { SemaforoPicoPlacaService } from './semaforo.service';

describe('SemaforoService', () => {
  let service: SemaforoPicoPlacaService;
  let injector: TestBed
  let httpMock: HttpTestingController
  const regla: Regla = {
    dia: {
      numero: 1,
      nombre: "Lunes"
    },
    ultimoDigito: [1, 2],
    horario: [
      {
        horaInicio: "07:00",
        horaFin: "09:30"
      },
      {
        horaInicio: "16:00",
        horaFin: "19:30"
      }
    ]
  
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        HttpClientModule
      ]
    });
    TestBed.configureTestingModule({});
    injector = getTestBed()
    httpMock = injector.get(HttpTestingController)
    service = TestBed.inject(SemaforoPicoPlacaService);
  });
 


  it('Debe ser creado', () => {
    expect(service).toBeTruthy();
  });
  it('Debe retornar un Observable<Regla[]>', () => {
    const servicio: SemaforoPicoPlacaService = TestBed.get(SemaforoPicoPlacaService)
    let mockReglas: Regla[] = [{
      dia: {
        numero: 1,
        nombre: "Lunes"
      },
      ultimoDigito: [1, 2],
      horario: [
        {
          horaInicio: "07:00",
          horaFin: "09:30"
        },
        {
          horaInicio: "16:00",
          horaFin: "19:30"
        }
      ]
    },
    {
      dia: {
        numero: 2,
        nombre: "Martes"
      },
      ultimoDigito: [3, 4],
      horario: [
        {
          horaInicio: " 07:00",
          horaFin: "09:30"
        },
        {
          horaInicio: "16:00",
          horaFin: "19:30"
        }
      ]
    }]
    service.cargarCatalogoReglasPicoPlaca().subscribe((reglas) => {
      expect(reglas.length).toBe(2)
      expect(reglas).toEqual(mockReglas)
    })
    const req = httpMock.expectOne('./assets/reglasCatalogo.json')
    expect(req.request.method).toBe('GET')
    req.flush(mockReglas)


  })
  


});
